# Copyright (C) Tristan Koch, All Rights Reserved.

FROM php:7.0.2-apache
MAINTAINER Tristan Koch tristan@tknetwork.de

# Dependencies
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    sudo gettext less vim-tiny \
    git nodejs npm \
    libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libpng12-dev \
  && ln -s /usr/bin/nodejs /usr/bin/node \
  && npm install -g grunt-cli \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

# PHP extensions & Apache modules
RUN docker-php-ext-install mbstring tokenizer pdo_mysql zip \
  && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-install gd \
  && a2enmod rewrite expires headers

# Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
