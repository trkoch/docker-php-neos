# php-neos

Docker image with PHP environment suitable for running Neos CMS applications.

Based on [php:7.0-apache](https://hub.docker.com/_/php/).

Includes

* PHP extensions (e.g. `pdo_mysql`)
* PHP Composer
* Node.js (and grunt) for asset compilation
